/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controlador;
import modelo.*;
import Vista.jifProductos;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import javax.swing.JOptionPane;

public class Controlador implements ActionListener{
    private dbProductos db;
    private jifProductos Vista;
    private boolean esActualizar;
    private int idProducto;
    
    //constructos

    public Controlador(jifProductos Vista, dbProductos db) {
        this.Vista = Vista;
        this.db = db; 
        
        //Escuchar el evento clic de los siguiente botones 
        Vista.btnBuscar.addActionListener(this);
        Vista.btnCancelar.addActionListener(this);
        Vista.btnCerrar.addActionListener(this);
        Vista.btnDeshabilitar.addActionListener(this);
        Vista.btnGuardar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        Vista.btnNuevo.addActionListener(this);
          
       }
    
    // metodos helpers
       public void limpiar(){
           Vista.txtCodigo.setText("");
           Vista.txtNombre.setText("");
           Vista.txtPrecio.setText("");
           Vista.jdcFecha.setDate(new Date());
       }
       public void cerrar(){
           int res = JOptionPane.showConfirmDialog(Vista, "Desea cerrar el sistema", "Productos", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
           if(res == JOptionPane.YES_OPTION){
           Vista.dispose();
            }
      }
       public void habilitar(){
           Vista.txtCodigo.setEnabled(true);
           Vista.txtNombre.setEnabled(true);
           Vista.txtPrecio.setEnabled(true);
           Vista.btnBuscar.setEnabled(true);
           Vista.btnDeshabilitar.setEnabled(true);
           Vista.btnNuevo.setEnabled(true);
          
       }
           public void deshabilitar(){
           Vista.txtCodigo.setEnabled(false);
           Vista.txtNombre.setEnabled(false);
           Vista.txtPrecio.setEnabled(false);
           Vista.btnBuscar.setEnabled(false);
           Vista.btnDeshabilitar.setEnabled(false);
           Vista.btnNuevo.setEnabled(false);
          
       }
           public  boolean validar(){
               boolean exito = true;
               
               if(Vista.txtCodigo.getText().equals("")
                   || Vista.txtNombre.getText().equals("")
                   || Vista.txtPrecio.getText().equals("")) exito = false;
               return exito ;
           }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource()==  Vista.btnLimpiar) this.limpiar();
        if(e.getSource()== Vista.btnCancelar) this.limpiar(); this.deshabilitar();
        if(e.getSource()== Vista.btnCerrar) this.cerrar();
        if(e.getSource()== Vista.btnNuevo) {this.limpiar(); this.habilitar(); this.esActualizar=false;  Vista.txtCodigo.requestFocus();}
       
        
        if(e.getSource()== Vista.btnDeshabilitar){
            Producto pro = new Producto();
            pro.setIdProducto(this.idProducto);
            int res = JOptionPane.showConfirmDialog(Vista, "Esta seguro de deshabilitar el registro", "Productos", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
            if(res == JOptionPane.YES_OPTION){
              try{
                  db.deshabilitar(pro);
                  
              }catch(Exception a){
                  JOptionPane.showMessageDialog(Vista, "Se produjo un error al deshabilitar"+ a.getMessage());
                  this.limpiar();
                  this.deshabilitar();
              }
          }

                
           
       }
        if(e.getSource() == Vista.btnGuardar){
           // Hacer el objeto de clase producto 
           Producto pro = new Producto ();
            //Validar
            if (Vista.txtCodigo.equals("")){
                JOptionPane.showMessageDialog(Vista, "Favpr de capturarr eñ cpdigo");

            }else{
                try{                
                    pro = (Producto) db.buscar(Vista.txtCodigo.getText());
                    if( pro.getIdProducto() !=0){
                        //aleluya si encontro                    
                        Vista.txtNombre.setText(pro.getNombre());
                        Vista.txtPrecio.setText((String.valueOf(pro.getPrecio())));
                        Vista.btnDeshabilitar.setEnabled(true);
                        Vista.btnGuardar.setEnabled(true);
                        this.esActualizar = true;
                        this.idProducto = pro.getIdProducto();
                        }else JOptionPane.showMessageDialog(Vista,"No se encotro el codigo" + Vista.txtCodigo.getText());
                    }catch(Exception a){
                        JOptionPane.showMessageDialog(Vista, "Surgio errror al buscar" + a.getMessage());
                    }    
            if(this.validar() == true){
                //todo bien 
                pro.setCodigo(Vista.txtCodigo.getText());
                pro.setNombre(Vista.txtNombre.getText());
                pro.setPrecio(Float.parseFloat(Vista.txtPrecio.getText()));
                pro.setFecha("2024-07-05");
                try{                   
                if(this.esActualizar == false){
                    
                
                db.insertar(pro);
                JOptionPane.showMessageDialog(Vista, "Se agrego con exito el producto con codigo" + Vista.txtCodigo);
                        this.limpiar();
                        this.deshabilitar();
                        //actualizar tabla
                }else{
                    //se actualizara
                    db.actualizar(pro);
                     JOptionPane.showMessageDialog(Vista, "Se actualizo con exito el producto con codigo" + Vista.txtCodigo);
                        this.limpiar();
                        this.deshabilitar();
                        // actualizar tabla
                    
                }
                }catch(Exception a ){
                    JOptionPane.showMessageDialog(Vista, "Surgio un eror al insertar Productos" + a.getMessage());
                }
                
            }else JOptionPane.showMessageDialog(Vista, "Falto Informacion");
        }
            
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
    
    
}
}
