/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package modelo;
import javax.swing.JOptionPane;

public class TestDbProducto {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         dbProductos db = new dbProductos();
        if(db.conectar()) {
            System.out.println("Fue posible conectarse");
        

          Producto pro = new Producto();
          pro.setCodigo("1006");
          pro.setNombre("Atun");
          pro.setPrecio(10.50f);
          pro.setStatus(1);

            try {
                db.insertar(pro);
                JOptionPane.showMessageDialog(null, "Se agrego con exito");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Surgio un error " + e.getMessage());
            }
             try {
                db.actualizar(pro);
                JOptionPane.showMessageDialog(null,"Se actualizo con exito");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Surgio un error" + e.getMessage());
            }
            
            try {
                db.habilitar(pro);
                JOptionPane.showMessageDialog(null,"Se habilito con exito");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Surgio un error" + e.getMessage());
            }
            
            try {
                db.deshabilitar(pro);
                JOptionPane.showMessageDialog(null,"Se deshabilito con exito");
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "Surgio un error" + e.getMessage());
            }
            try{
                if(db.siExiste(1)) JOptionPane.showMessageDialog(null,"Si existe");
                else JOptionPane.showMessageDialog(null, "No existe");
                }catch(Exception e){
                System.out.println("Surgio un error" + e.getMessage());
                
            }
            try{
                pro= (Producto) db.buscar("100");
                if(pro.getIdProducto() ==0) JOptionPane.showMessageDialog(null,"El producto buscado no existe");
                else  JOptionPane.showMessageDialog(null, "Producto" + pro.getNombre() + "precio" + pro.getPrecio());
            }catch(Exception e ){
                System.out.println("Surgio un error" + e.getMessage());
                
            }

    }
 }        
}
    

