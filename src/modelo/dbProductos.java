
package modelo;
import java.util.ArrayList;

/**
 *
 * @author emili
 */
public class dbProductos extends dbManejador implements persistencia {
    public dbProductos() {
        super();
    }

    @Override
    public void insertar(Object object) throws Exception {
        Producto pro = new Producto();
        pro = (Producto) object;

        String consulta = "";
        consulta = "Insert into productos(codigo,nombre,fecha,precio,status) values(?,?,?,?,?)";
        this.sqlConsulta = this.conexion.prepareStatement(consulta);
        this.sqlConsulta.setString(1, pro.getCodigo());
        this.sqlConsulta.setString(2, pro.getNombre());
        this.sqlConsulta.setString(3, "2024-06-20");
        this.sqlConsulta.setFloat(4, pro.getPrecio());
        this.sqlConsulta.setInt(5, pro.getStatus());
        
        if (this.conectar()) {
            this.sqlConsulta.executeUpdate();
            this.desconectar();
        }
    }

    @Override
    public void actualizar(Object object) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void habilitar(Object object) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public void deshabilitar(Object object) throws Exception {
       
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    
    }

    @Override
    public boolean siExiste(int id) throws Exception {
   //  throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    boolean exito = false;
            String consulta ="";
            consulta = "select*from productos where idProductos = ? and status =0";
            if(this.conectar()){
                this.sqlConsulta = this.conexion.prepareStatement(consulta);
                this.sqlConsulta.setInt(1,id);
                    registros = this.sqlConsulta.executeQuery();
                    if(registros.next())exito = true;
                this.desconectar();
            }
            return exito;
        }

    

    @Override
    public ArrayList lista() throws Exception {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        ArrayList listaProducto = new ArrayList<Producto>();
        String consulta = "select * from productos where status = 0 order by codigo";
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            registros = this.sqlConsulta.executeQuery();
            
            while(registros.next()){
                Producto pro = new Producto();
                    pro.setCodigo(registros.getString("codigo"));
                    pro.setNombre(registros.getString("nombre"));
                    pro.setPrecio(registros.getInt("precio"));
                    pro.setFecha(registros.getString("fecha"));
                    pro.setIdProducto(registros.getInt("idProductos"));
                    pro.setStatus(registros.getInt("status"));
                    
                    listaProducto.add(pro);
                    
            }
        }
            this.desconectar();
            return listaProducto;
    }

    @Override
    public Object buscar(String codigo) throws Exception {
        Producto pro = new Producto();
        String consulta = "Select * from productos where codigo = ? and status = 0";
        
        if(this.conectar()){
            this.sqlConsulta = this.conexion.prepareStatement(consulta);
            this.sqlConsulta.setString(1, codigo);
            
            registros = this.sqlConsulta.executeQuery();
                
                if(registros.next()){
                    pro.setCodigo(codigo);
                    pro.setNombre(registros.getString("nombre"));
                    pro.setPrecio(registros.getInt("precio"));
                    pro.setFecha(registros.getString("fecha"));
                    pro.setIdProducto(registros.getInt("idProductos"));
                    pro.setStatus(registros.getInt("status"));
                    
                        
                    }
                this.desconectar();
                return pro;
                }
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBo
                    
    }
    }


